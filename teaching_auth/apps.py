from django.apps import AppConfig


class TeachingAuthConfig(AppConfig):
    name = 'teaching_auth'
