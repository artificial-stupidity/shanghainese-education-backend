# rest framework
from rest_framework import generics, permissions
# local
from .models import Client
from .serializers import ClientSerializer, ClientRegistrationSerializer
from .permissions import IsAnonymousOrReadOnly



class ClientRegistration(generics.CreateAPIView):
    '''
    Create a new account
    '''
    permission_classes = (IsAnonymousOrReadOnly,)
    serializer_class = ClientRegistrationSerializer

    def perform_create(self, serializer):
        # Assumption that the serializer.is_valid() has been ran before perfore_create
        user = Client.objects.create_user(
            **serializer.validated_data
        )

class ClientSelf(generics.RetrieveUpdateAPIView):
    '''
    Retrieve, or update the current client that is logged in
    '''
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ClientSerializer

    def get_object(self):
        return self.request.user


