from django.conf.urls import url
from django.urls import include, path

from . import views
from rest_framework.authtoken import views as authtoken_views


urlpatterns = [
    path('register/', views.ClientRegistration.as_view(), name='client-register'),
    path('client/', views.ClientSelf.as_view(), name='client'),
    # for login/logout redirect pages
    path('client/', include('rest_framework.urls')),
    path('auth-token/', authtoken_views.obtain_auth_token, name='client-tokenauth')
]
