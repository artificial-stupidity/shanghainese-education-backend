# django
from django.contrib.auth.password_validation import validate_password
# rest framework
from rest_framework import serializers
# local
from .models import Client

class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = ('id', 'username', 'email', 'is_email_verified')
        read_only_fields = ('email', 'is_email_verified')

    def create(self, validated_data):
        '''
        Client serializer is only used in user information change,
        therefore, creation is not allowed
        '''
        pass


    def validate_password(self, value):
        if not value:
            raise serializers.ValidationError("Password could not be null.")


class ClientRegistrationSerializer(serializers.ModelSerializer):
    '''
    A model serializer for registration information
    '''
    class Meta:
        model = Client
        fields = ('id', 'username', 'email', 'password')

