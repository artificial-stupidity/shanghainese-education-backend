from rest_framework.exceptions import APIException

class HasActiveTrainingSession(APIException):
    '''
    Exception on creating a new TrainingSession when already have an active one
    '''
    pass

class NoDialogs(APIException):
    '''
    Exceptions there is not selected dialogs
    '''
    pass