from django.apps import AppConfig


class TeachingRestConfig(AppConfig):
    name = 'teaching_rest'
