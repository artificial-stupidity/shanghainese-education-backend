from django.urls import path
from teaching_rest import views

category_list = views.CategoryViewset.as_view({
    'get': 'list',
    'post':'create'
})

category_detail = views.CategoryViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

dialog_list = views.DialogViewset.as_view({
    'get': 'list',
    'post': 'create'
})

dialog_detail = views.DialogViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

trainingsession_list = views.TrainingSessionViewset.as_view({
    'get': 'list',
    'post': 'create'
})

trainingsession_detail = views.TrainingSessionViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

urlpatterns = [
    path('category/', category_list, name='category-list'),
    path('category/<int:pk>/', category_detail, name='category-details'),
    path('dialog/', dialog_list, name='dialog-list'),
    path('dialog/<int:pk>/', dialog_detail, name='dialog-details'),
    path('session/', trainingsession_list, name='trainingsession-list'),
    path('session/<int:pk>/', trainingsession_detail, name='trainingsession-details'),
    path('training/', views.TrainingList.as_view(), name="training-list"),
    path('training/<int:pk>/', views.TrainingDetails.as_view(), name='training-details'),
    path('evaluation/<int:pk>/', views.EvaluationDetails.as_view(), name='evaluation-details')
]
