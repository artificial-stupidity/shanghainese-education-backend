import os
import uuid
from datetime import date

from django.conf import settings
from django.db import models
from django.dispatch import receiver

from .validators import validate_file_extension


class Category(models.Model):
    name = models.CharField(max_length=200)
    parent = models.ForeignKey('self', related_name="subcategories", on_delete=models.CASCADE, blank=True, null=True)


def dialog_audio_md5_rename(instance, filename):
    '''
    Provide rename for uploaded audio from Dialog
    :param instance: Dialog instance
    :param filename: the uploaded file name
    :return: the relative path to store uploaded file
    '''
    _, ext = os.path.splitext(filename)
    return "dialog/{0}-{1}{2}".format(date.today().strftime("%y%m%d"), uuid.uuid4(), ext)


class Dialog(models.Model):
    difficulty = models.PositiveSmallIntegerField()
    category = models.ForeignKey('Category', related_name="dialogs", on_delete=models.CASCADE, blank=True, null=True)
    romanized_phones = models.CharField(max_length=200, blank=True, null=True)
    chinese_translation = models.CharField(max_length=200, blank=True, null=True)
    english_translation = models.CharField(max_length=200, blank=True, null=True)
    shanghaiese_translation = models.CharField(max_length=200, blank=True, null=True)
    audio = models.FileField(upload_to=dialog_audio_md5_rename, validators=[validate_file_extension])


@receiver(models.signals.post_delete, sender=Dialog)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `Dialog` object is deleted.
    """
    if instance.audio:
        if os.path.isfile(instance.audio.path):
            os.remove(instance.audio.path)


class TrainingSession(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    difficulty = models.PositiveSmallIntegerField()
    grade = models.PositiveSmallIntegerField(default=0)
    isActive = models.BooleanField(default=True)
    isCompleted = models.BooleanField(default=False)
    beginTime = models.DateTimeField(auto_now_add=True)
    completeTime = models.DateTimeField(blank=True, null=True)


class Training(models.Model):
    training_session = models.ForeignKey('TrainingSession', related_name='trainings', on_delete=models.CASCADE)
    dialog = models.ForeignKey('Dialog', on_delete=models.CASCADE)


class Evaluation(models.Model):
    training = models.OneToOneField('Training', related_name='evaluation', on_delete=models.CASCADE)
    translation = models.CharField(max_length=200, blank=True, null=True)
    isUpdated = models.BooleanField(default=False)
    grade = models.IntegerField(default=0, blank=True, null=True)
    comparison = models.CharField(max_length=1000, blank=True, null=True)
