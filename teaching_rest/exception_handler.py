from rest_framework.views import exception_handler
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

def custom_exception_handler(exc, context):
    '''
    Customized exception handler that converts raised exceptions to json response
    :param exc: exceptions that is raised from the API
    :param context: the informations for the exception
    :return: a response to user indicating the exception
    '''
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)
    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code

    return response
