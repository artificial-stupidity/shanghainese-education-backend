import os
import string

from diff_match_patch import diff_match_patch as DMP
from django.conf import settings
from django.http import FileResponse
from rest_framework import generics, viewsets
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response
from zhon import hanzi

from teaching_rest.models import (
    Category, Dialog, TrainingSession, Training, Evaluation
)
from teaching_rest.serializers import (
    CategorySerializer,
    UploadDialogSerializer,
    DisplayDialogSerializer,
    TrainingSessionSerializer,
    DisplayTrainingSerializer,
    EvaluationSerializer
)
from .exceptions import HasActiveTrainingSession, NoDialogs


class CategoryViewset(viewsets.ModelViewSet):
    serializer_class = CategorySerializer

    def get_queryset(self):
        if self.action == 'list':
            return Category.objects.filter(parent=None)
        return Category.objects.all()


class DialogViewset(viewsets.ModelViewSet):
    queryset = Dialog.objects.all()

    def get_serializer_class(self):
        if self.action == 'create':
            return UploadDialogSerializer
        return DisplayDialogSerializer

    def retrieve(self, request, *args, **kwargs):
        '''
        Retrieve the given instance when pk is supplied.
        If parameter alt=media is given,
            that is, when the request is "GET /dialog/<pk>?alt=media",
            file download is provided
        '''
        if self.request.query_params.get('alt') == 'media':
            instance = self.get_object()
            response = FileResponse(instance.audio.open())
            response['Content-Length'] = instance.audio.size
            response['Content-Disposition'] = \
                'attachment; filename="%s"' % os.path.basename(instance.audio.name)
            return response
        return super(DialogViewset, self).retrieve(request, *args, **kwargs)


class TrainingSessionViewset(viewsets.ModelViewSet):
    serializer_class = TrainingSessionSerializer

    def get_queryset(self):
        '''
        obtain the queryset based on the user
        '''
        return TrainingSession.objects \
            .filter(user=self.request.user)

    def perform_create(self, serializer):
        '''
        Perform new training session creation
        '''
        training_sessions = TrainingSession.objects.filter(user=self.request.user, isActive=True)
        if training_sessions.exists():
            raise HasActiveTrainingSession("An active session already exists for this user.")
        del training_sessions

        training_session = TrainingSession.objects.create(
            user=self.request.user,
            **serializer.validated_data
        )

        # Select up to N dialog for the Trainings
        dialog_queryset = \
            Dialog.objects \
                .filter(difficulty=training_session.difficulty).order_by("?")
        cnt = dialog_queryset.count()
        if cnt == 0:
            raise NoDialogs("Not enough dialogs for practice.")
        elif cnt > settings.NUM_TRAINING_PER_SESSION:
            dialog_queryset = dialog_queryset[:settings.NUM_TRAINING_PER_SESSION]

        # create Training and Evaluations
        for dialog in dialog_queryset:
            training = Training.objects.create(
                training_session=training_session,
                dialog=dialog
            )
            evaluation = Evaluation.objects.create(
                training=training
            )

    def perform_destroy(self, instance):
        '''
        Destroy does not delete the instance, instead, it set the instance to false
        '''
        instance.isActive = False
        instance.save()


class TrainingList(generics.ListAPIView):
    '''
    List all the past trainings
    '''
    serializer_class = DisplayTrainingSerializer

    def get_queryset(self):
        '''
        Obtain the queryset based on the user
        If parameter 'isActive' is specified,
            that is, when the request is 'GET /training?isActive=True"
            The list of training that is associated with an active
            TrainingSession of the user is returned
        '''
        queryset = Training.objects.filter(
            training_session__user=self.request.user
        )
        is_active = self.request.query_params.get('isActive', None)
        if is_active in ('True', 'False'):
            queryset = queryset.filter(training_session__isActive=is_active)
        return queryset


class TrainingDetails(generics.RetrieveUpdateAPIView):
    '''
    List all the past trainings
    '''

    serializer_class = DisplayTrainingSerializer

    def get_queryset(self):
        '''
        Obtain the queryset based on the user
        '''
        return Training.objects.filter(
            training_session__user=self.request.user,
        )


ALL_PUNCTUATIONS = string.punctuation + hanzi.punctuation


def remove_punctuations(text: str):
    '''
    Remove all punctuation from the input text (incl Chinese punctuations)
    '''
    return text.translate(str.maketrans("", "", ALL_PUNCTUATIONS))


dmp = DMP()


def score_levenshtein(text1, text2):
    '''
    Compute the levenshtein
    '''
    text1 = remove_punctuations(text1)
    text2 = remove_punctuations(text2)
    diffs = dmp.diff_main(text1, text2)
    levin = dmp.diff_levenshtein(diffs)
    return 1 / (1 + levin)


def score_length(dialog, translation):
    '''
    Compute the length difference score
    A ratio between [0 - 1] is returned
    '''
    diff = abs(len(translation) - len(dialog))
    if diff <= len(dialog):
        return (len(dialog) - diff) / len(dialog)
    else:
        return 0


def score_equivalence(dialog, translation):
    '''
    Compute the equivalence ratio of how many character in the dialog is in transcrption.
    A ratio between [0 - 1] is returned
    '''
    dialog = remove_punctuations(dialog)
    translation = remove_punctuations(translation)
    translation_charset = set(translation)
    dialog_charset = set(dialog)
    return sum(map(lambda char: char in translation_charset, dialog_charset)) / len(dialog_charset)


def total_score(dialog: str, translation: str):
    '''
    Compute the total score given the dialog string and translation script
    '''
    if score_equivalence(dialog, translation) == 0:
        return 0
    methods = [score_equivalence, score_length, score_levenshtein]
    portion = [40, 10, 50]
    vanilla_score = map(lambda p, m: p * m(dialog, translation), portion, methods)
    return sum(vanilla_score)


class EvaluationDetails(generics.RetrieveUpdateAPIView):
    serializer_class = EvaluationSerializer

    def get_queryset(self):
        return Evaluation.objects.filter(training__training_session__user=self.request.user)

    def update(self, request, *args, **kwargs):
        '''
        Perform POST/PATCH update to the evaluation.
        The translation is uploaed and the score is evaluated.
        Post can only be executed once.
        '''
        instance = self.get_object()
        if not instance.isUpdated:
            super(EvaluationDetails, self).update(request, args, kwargs)
            instance = self.get_object()
            instance.isUpdated = True
            translation = instance.translation
            dialog = instance.training.dialog.shanghaiese_translation
            instance.grade = total_score(dialog, translation)
            instance.save()
            return Response(self.get_serializer(instance).data)
        else:
            raise MethodNotAllowed(self.request.method, detail='Cannot update evaluation twice.')
