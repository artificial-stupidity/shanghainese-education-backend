from django.test import TestCase
from teaching_rest.models import Category, Dialog, TrainingSession, Training, Evaluation
from django.db import transaction, IntegrityError


class CategoryTestCase(TestCase):
  def setUp(self):
    Category.objects.create(name="lion")
    Category.objects.create(name="cat")

  def test_categories_can_speak(self):
    """Categories with different names"""
    lion = Category.objects.get(name="lion")
    cat = Category.objects.get(name="cat")
    self.assertEqual(lion.name, 'lion')
    self.assertEqual(cat.name, 'cat')


class DialogTestCase(TestCase):
  @transaction.atomic
  def test_create_invalid_dialogs(self):
    try:
      with transaction.atomic():
        Dialog.objects.create(difficulty=None)
        self.fail("The program failed to detect an null exception here")
    except IntegrityError:
      pass

  def setUp(self):
    Dialog.objects.create(romanized_phones="one", difficulty=4)
    Dialog.objects.create(romanized_phones="two", difficulty=4)
    Dialog.objects.create(romanized_phones="", difficulty=4)
    Dialog.objects.create(romanized_phones=None, difficulty=4)
    Dialog.objects.create(chinese_translation="yige", difficulty=4)
    Dialog.objects.create(chinese_translation="liangge", difficulty=4)
    Dialog.objects.create(chinese_translation="", difficulty=4)
    Dialog.objects.create(chinese_translation=None, difficulty=4)
    Dialog.objects.create(english_translation="first", difficulty=4)
    Dialog.objects.create(english_translation="second", difficulty=4)
    Dialog.objects.create(english_translation="", difficulty=4)
    Dialog.objects.create(english_translation=None, difficulty=4)

  def test_dialog_difficulty(self):
    Dialog.objects.create(difficulty=1)
    Dialog.objects.create(difficulty=4)
    d1 = Dialog.objects.get(difficulty=1)
    d4 = Dialog.objects.filter(difficulty=4)
    self.assertEqual(d1.difficulty, 1)
    self.assertEqual(d4[0].difficulty, 4)
    try:
      dnull = Dialog.objects.get(difficulty=None)
      self.fail("The program failed to detect an null exception here")
    except Dialog.DoesNotExist:
      pass
    # self.assertRaises(FooException, Dialog.objects.get, difficulty=None)

  def test_dialog_romanized_phones(self):
    one = Dialog.objects.get(romanized_phones="one")
    two = Dialog.objects.get(romanized_phones="two")
    blank = Dialog.objects.get(romanized_phones="")
    null = Dialog.objects.filter(romanized_phones=None)
    self.assertEqual(one.romanized_phones, "one")
    self.assertEqual(two.romanized_phones, "two")
    self.assertEqual(blank.romanized_phones, "")
    self.assertEqual(null[0].romanized_phones, None)

  def test_dialog_chinese_translation(self):
    yige = Dialog.objects.get(chinese_translation="yige")
    liangge = Dialog.objects.get(chinese_translation="liangge")
    blank = Dialog.objects.get(chinese_translation="")
    null = Dialog.objects.filter(chinese_translation=None)
    self.assertEqual(yige.chinese_translation, "yige")
    self.assertEqual(liangge.chinese_translation, "liangge")
    self.assertEqual(blank.chinese_translation, "")
    self.assertEqual(null[0].chinese_translation, None)

  def test_dialog_english_translation(self):
    first = Dialog.objects.get(english_translation="first")
    second = Dialog.objects.get(english_translation="second")
    blank = Dialog.objects.get(english_translation="")
    null = Dialog.objects.filter(english_translation=None)
    self.assertEqual(first.english_translation, "first")
    self.assertEqual(second.english_translation, "second")
    self.assertEqual(blank.english_translation, "")
    self.assertEqual(null[0].english_translation, None)