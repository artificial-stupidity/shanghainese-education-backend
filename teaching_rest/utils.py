import os

import pandas as pd
from django.core.files import File

from teaching_rest.models import Category, Dialog

def load_to_db(df : pd.DataFrame):
    df 
    for _, row in df.iterrows():
        c, created = Category.objects.get_or_create(
            name=row['category_EN'],
            defaults={'parent': None}
        )
        s, created = Category.objects.get_or_create(
            name=row['subcategory_EN'],
            defaults={'parent': c}
        )
        d = Dialog(
            difficulty=1 if row['length'] <= 5 else 2 if row['length'] <= 10 else 3,
            category=s,
            romanized_phones=row['phones'],
            chinese_translation=row['mandarin'],
            english_translation=row['english'],
            shanghaiese_translation=row['shanghainese'],
            audio=File(
                open(
                    os.path.join('test', 'data', os.path.basename(row['path'])),
                    'rb'
                )
            )
        )
        d.save()
