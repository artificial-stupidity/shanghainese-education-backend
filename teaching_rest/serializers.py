from rest_framework import serializers

from teaching_rest.models import Category, Dialog, TrainingSession, Training, Evaluation


class MiniCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class DisplayDialogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dialog
        exclude = ('audio',)


class UploadDialogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dialog
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    subcategories = MiniCategorySerializer(many=True, read_only=True)
    dialogs = DisplayDialogSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = '__all__'


class EvaluationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Evaluation
        fields = '__all__'
        read_only_fields = ('training', 'isUpdated', 'grade', 'comparison')


class DisplayTrainingSerializer(serializers.ModelSerializer):
    evaluation = EvaluationSerializer(many=False, read_only=True)

    class Meta:
        model = Training
        fields = '__all__'


class TrainingSessionSerializer(serializers.ModelSerializer):
    trainings = DisplayTrainingSerializer(many=True, read_only=True)

    class Meta:
        model = TrainingSession
        read_only_fields = ('id', 'grade', 'isActive', 'isCompleted', 'beginTime', 'completeTime', 'trainings')
        exclude = ('user',)
