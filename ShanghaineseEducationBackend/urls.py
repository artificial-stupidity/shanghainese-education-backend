from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from .views import api_root

urlpatterns = [
    path(r'', include('teaching_auth.urls')),
    path(r'', include('teaching_rest.urls')),
    path(r'admin/', admin.site.urls),
    path(r'', api_root)
]
