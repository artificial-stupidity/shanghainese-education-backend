from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'register': reverse('client-register', request=request, format=format),
        'client': reverse('client', request=request, format=format),
        'tokenauth' : reverse('client-tokenauth', request=request, format=format),
        'category' : reverse('category-list', request=request, format=format),
        'dialog': reverse('dialog-list', request=request, format=format),
        'session': reverse('trainingsession-list', request=request, format=format),
        'training': reverse('training-list', request=request, format=format),
    })
